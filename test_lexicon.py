#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from unittest import TestCase
import pandas as pd
import re


class TestLexicon(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.lexicon = pd.read_csv("RussianNouns.csv", index_col=0)
        cls.cols = ["sg.nom", "sg.acc", "sg.gen", "sg.dat", "sg.ins",
                    "sg.loc", "sg.loc2", "pl.nom", "pl.acc",
                    "pl.gen", "pl.dat", "pl.ins", "pl.loc",
                    "translation", "gender",
                    "animacy", "absolute rank", "noun rank", "DATR-node"]
        cls.form_cols = cls.cols[:13]

    def test_columns(self):
        self.assertEqual(self.cols, self.lexicon.columns.tolist())

    def test_alphabet(self):
        legal_chars = {'a', 'd', 'j', 'u', 't', 'á', 'n', 'r', 'ʲ', 'e', 's',
                       'o', 'ó', 'm', 'g', 'é', 'i', 'k', 'l', 'z', 'ʦ', 'ʨ',
                       '#', 'D', 'E', 'F', 'p', 'í', 'x', 'ú', 'f', 'v', 'b',
                       'ʃ', 'ʒ', 'ɕ', 'ː'}
        unique_chars = set(self.lexicon[self.form_cols]
                           .applymap(list)
                           .melt()
                           .explode("value")["value"]
                           .unique()
                           .tolist())
        self.assertSetEqual(unique_chars, legal_chars,
                            "Illegal characters found")

    def test_palatalisation(self):
        """
        ʲ, ɕː, ʨ, j, ʒ, ʃ, ʦ and vowels  are never followed by ʲ
        either they are already palatal, or they can't be
        """

        def has_impossible_palatalization(form):
            return re.match("[ʲːʨjʒʃɕʦaáeéiíoóuú]ʲ", form) is not None

        illegal_palatal = self.lexicon[self.form_cols].applymap(
            has_impossible_palatalization)
        self.assertFalse(illegal_palatal.all().all(),
                         "There are illegal palatals in: {}"
                         .format(self.lexicon[self.form_cols][illegal_palatal]))

        e_after_pal = self.lexicon[self.form_cols].applymap(lambda f: "ʲe" in f)
        self.assertFalse(e_after_pal.all().all(),
                         "There are unstressed e after a palatar: {}"
                         .format(self.lexicon[self.form_cols][e_after_pal]))

    def test_specific_examples(self):

        # Checking that final -e after palatals is conserved
        values = self.lexicon.loc["bábuʃka",['sg.loc2', 'sg.dat', 'sg.loc']].to_list()
        expected = ['bábuʃkʲe'] * 3
        self.assertListEqual(values, expected)

    def test_stress(self):

        def stress_present(form):
            if form == "#DEF#":
                return True
            for V in ["á", "é", "í", "ó", "ú"]:
                if V in form:
                    return True
            return False

        has_stress = self.lexicon[self.form_cols].applymap(stress_present)

        self.assertTrue(has_stress.all().all(),
                        "There are forms without stress: {}"
                        .format(self.lexicon[self.form_cols][~has_stress]))
