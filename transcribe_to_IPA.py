#!/usr/bin/env python
# coding: utf-8
"""Transcribe from romanisation to IPA.

Author: Sacha Beniamine.
"""

import pandas as pd
import re
import logging

def multireplace(dic, text, greedy=True):
    if greedy:
        keys = sorted(dic, key=lambda x: -len(x))
    else:
        keys = sorted(dic, key=lambda x: len(x))
    pattern = re.compile("|".join(re.escape(k) for k in keys))
    result = pattern.sub(lambda m: dic[m.group(0)], text)
    return result


def attribute_stress(form):
    """ Mark stress on vowels as the character "'"

    - @\" and -& mark stress, they are placed after the syllable:

        zvonkom@" -> zvonkóm
        l'ud'ej-& -> l'ud'éj

    - if no stress is marked then it should be on the first syllable.
    """

    def new_stress(form, i):
        accent_on_vowel = {"a": "á", "e": "é", "i": "í", "o": "ó", "u": "ú"}
        return form[:i] + accent_on_vowel[form[i]] + form[i + 1:]

    if form != "#DEF#":
        if '@"' in form or "-&" in form:
            stressed_syll = re.compile(r"[aeiou][^aeiou]*?(@\"|-&)")
        else:
            stressed_syll = re.compile(r"[aeiou]")

        i = stressed_syll.search(form).start()
        form = multireplace({"-&": "", '@"': ""}, form)
        return new_stress(form, i)
    return form


def palatal_C(form):
    """ Apply all palatal rules"""

    # Direct notation rule
    form = multireplace({"'": "ʲ", "@'": "ʲ"}, form)

    # Palatalization and digraphs rules
    form = multireplace({

        # ch, shch and c transcription. Can't be palatalized.
        "ch": "ʨ", "chʲ": "ʨ",
        "shch": "ɕː", "shchʲ": "ɕː",
        "c": "ʦ", "cʲ": "ʦ",

        # j can't be palatalized.
        "jʲ": "j",

        # Final e is pronounced "i"
        "ije$": "iji",

        # i palatalization of velars (even when stressed)
        "ki": "kʲi",
        "gi": "gʲi",
        "xi": "xʲi",
        "kí": "kʲí",
        "gí": "gʲí",
        "xí": "xʲí",

        # Simple translations
        "zh": "ʒ",
        "sh": "ʃ"}, form)

    # Palatalization of cons before e (only if the consonant is palatalizable)
    form = re.compile(r"(?<=[ksxgflmnprstvzbd])([ée])").sub(r"ʲ\1", form)

    # Unstressed e is o after a palatal #TODO: this has exceptions.
    # form = re.compile(r"(?<=[ʲːʨj])e").sub(r"o", form)

    return form


def manual_corrections(data, trans_cols):
    """ Corrects (in place) a few errors which were found manually. """

    # The nominative sg is correct, but the other forms should not have a superscript j after z
    # (i.e. when z is followed directly be l). This error occurred in the original.
    all_but_nom = ["sg acc", "sg gen", "sg dat", "sg ins", "sg loc", "sg loc2", "pl nom", "pl acc", "pl gen", "pl dat",
                   "pl ins", "pl loc"]
    data[all_but_nom] = data[all_but_nom].applymap(lambda x: x.replace("zʲl", "zl"))

    # others
    data[trans_cols] = data[trans_cols].applymap(lambda x: x.replace("ʨelovʲéʨestvoo", "ʨelovʲéʨestvo"))
    data.iloc[463, [0, 1, 2, 10]] = ["krʲéslo", "krʲéslo", "krʲéslo",
                                     "krʲésʲel"]  # was: krʲésʲelo   krʲésʲelo   krʲésʲelo

    # 121,ʨelovʲéʨestvo,ʨelovʲéʨestvo,ʨelovʲéʨestvo,ʨelovʲéʨestvoa,ʨelovʲéʨestvou,ʨelovʲéʨestvom,ʨelovʲéʨestvoe,
    # ʨelovʲéʨestvoe,ʨelovʲéʨestvoa,ʨelovʲéʨestvoa,ʨelovʲéʨestvo,ʨelovʲéʨestvoam,ʨelovʲéʨestvoamʲi
    #
    # should be :
    #
    # 121 ʨelovʲéʨestvo   ʨelovʲéʨestvo   ʨelovʲéʨestvo   ʨelovʲéʨestva   ʨelovʲéʨestvu   ʨelovʲéʨestvom  ʨelovʲéʨestvʲe
    # ʨelovʲéʨestvʲe  ʨelovʲéʨestva   ʨelovʲéʨestva   ʨelovʲéʨestv    ʨelovʲéʨestvam  ʨelovʲéʨestvamʲi    ʨelovʲéʨestvax
    data.iloc[121, :] = ["ʨelovʲéʨestvo", "ʨelovʲéʨestvo", "ʨelovʲéʨestvo", "ʨelovʲéʨestva", "ʨelovʲéʨestvu",
                         "ʨelovʲéʨestvom", "ʨelovʲéʨestvʲe", "ʨelovʲéʨestvʲe", "ʨelovʲéʨestva", "ʨelovʲéʨestva",
                         "ʨelovʲéʨestv", "ʨelovʲéʨestvam", "ʨelovʲéʨestvamʲi", "ʨelovʲéʨestvax", "humanity", "neut",
                         "inanimate", "458", "218", "Ch'elovech'estvo"]

    # 950,pʃenʲíʦaa,pʃenʲíʦaa,pʃenʲíʦau,pʃenʲíʦai,pʃenʲíʦae,pʃenʲíʦaoj,pʃenʲíʦae,pʃenʲíʦae,pʃenʲíʦai,pʃenʲíʦai,pʃenʲíʦa,pʃenʲíʦaam,pʃenʲíʦaamʲi
    # should be
    # 950     pʃenʲíʦa    pʃenʲíʦa    pʃenʲíʦu    pʃenʲíʦi    pʃenʲíʦe    pʃenʲíʦoj   pʃenʲíʦe    pʃenʲíʦe    pʃenʲíʦi
    # pʃenʲíʦi    pʃenʲíʦ     pʃenʲíʦam   pʃenʲíʦamʲi     pʃenʲíʦax

    data.iloc[950, :] = ["pʃenʲíʦa", "pʃenʲíʦa", "pʃenʲíʦu", "pʃenʲíʦi", "pʃenʲíʦe", "pʃenʲíʦoj", "pʃenʲíʦe",
                         "pʃenʲíʦe", "pʃenʲíʦi", "pʃenʲíʦi", "pʃenʲíʦ", "pʃenʲíʦam", "pʃenʲíʦamʲi", "pʃenʲíʦax",
                         "wheat", "fem", "inanimate", "485", "243", "Pshen'ica"]




def main(args):
    """Transcribe from romanisation to IPA.
    """
    logging.basicConfig(level=logging.INFO)
    data = pd.read_csv(args.input, index_col=0)
    data.head()

    # Columns to be transcribed
    trans_cols = ["lexeme", "sg nom", "sg acc", "sg gen", "sg dat", "sg ins", "sg loc", "sg loc2", "pl nom", "pl acc",
                  "pl gen", "pl dat", "pl ins", "pl loc"]

    logging.info("Removing unnecessary chars...")
    deletion = {" ": "", "^": "", "_": "", "(u)": "", "( u )": ""}
    data[trans_cols] = data[trans_cols].applymap(lambda x: multireplace(deletion, x.rstrip(".")))
    logging.info("Writing undefined as #DEF#...")
    data.replace("undefined", "#DEF#", inplace=True)
    logging.info("Writing stress as '...")
    data[trans_cols] = data[trans_cols].applymap(attribute_stress)
    logging.info("Transcribing palatals...")
    data[trans_cols] = data[trans_cols].applymap(palatal_C)
    logging.info("Making manual corrections...")
    manual_corrections(data, trans_cols)
    data.set_index("lexeme", inplace=True)
    cols = list(data.columns)
    data.columns = [".".join(x.split(" ")) for x in cols[:13]] + cols[13:]
    data.to_csv(args.output)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description=main.__doc__, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("input", help="original file", type=str)
    parser.add_argument("output", help="where to write the result",  type=str)
    main(parser.parse_args())
